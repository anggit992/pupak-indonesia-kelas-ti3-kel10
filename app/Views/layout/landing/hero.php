<!-- Hero Section -->
    <section id="hero">
      <div class="container h-100">
        <div class="row h-100">
          <div class="col-md-6 hero-tagline">
            <br />
            <br />
            <p><span class="fw-bold">Hai</span>, Selamat Datang!</p>
            <h1>Temukan Beragam Pupuk Tanaman Berkualitas Disini</h1>
            <h4>
              <span class="fw-bold">Solusi Pertanian</span> Dengan Kebutuhan
              Konsumen
            </h4>
            <p>
              <a href="#rekomendasi" class="button-lg-primary">Temukan Pupuk</a>
            </p>
          </div>
        </div>

        <img
          src="<?= base_url('assets/images/foto1.png') ?>"
          alt=""
          class="position-absolute end-0 bottom-0 img-hero"
        />
      </div>
    </section>
    <!-- Hero Section End-->