    <footer class="d-flex align-items-center position-relative">
      <div class="container-fluid">
        <div class="container">
          <div class="row">
            <div class="col-md-7 d-flex align-items-center">
              <img src="<?= base_url('assets/images/Pupak Indonesia.png') ?>" alt="" />
            </div>
            <div class="col-md-5 d-flex justify-content-evenly">
              <a href="#hero">Beranda</a>
              <a href="#layanan">Layanan</a>
              <a href="#rekomendasi">Produk</a>
              <a href="#contact">Kontak</a>
            </div>
          </div>

          <div class="row position-absolute wrapper start-50 translate-middle">
            <div class="col-12"></div>
            <p>&copy; 2023. <b>Pupak Indonesia</b> All Rights Reserved</p>
          </div>
        </div>
      </div>
    </footer>

